#include <stdio.h>
#include <stdlib.h>

int main()
{
    int arr[100]; /* maximum jumlah array 100 */
    int i, N;

    printf("Masukan jumlah array : ");
    scanf("%d", &N);

    /* masukkan elemen di dalam array */
    printf("Masukkan %d element di dalam array : ", N);
    for (i = 0; i < N; i++)
    {
        scanf("%d", &arr[i]);
    }

    /* cetak semua elemen pada array */
    printf("\nElement pada array adalah : ");
    for (i = 0; i < N; i++)
    {
        printf("%d, ", arr[i]);
    }

    return 0;
}
